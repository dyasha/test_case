from typing import Optional

from pydantic import BaseSettings, EmailStr


class Settings(BaseSettings):
    app_title: str
    database_url: str
    secret: str = "SECRET"
    first_superuser_email: Optional[EmailStr] = None
    first_superuser_password: Optional[str] = None
    first_superuser_name: Optional[str] = None
    first_superuser_last_name: Optional[str] = None

    class Config:
        env_file = ".env"


settings = Settings()
