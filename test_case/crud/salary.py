from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from test_case.crud.base import CRUDBase
from test_case.models.salary import Salary
from test_case.models.user import User
from test_case.schemas.salary import SalaryGET
from test_case.schemas.user import UserRead


class SalaryCRUD(CRUDBase):
    async def get_by_user(
        self,
        session: AsyncSession,
        user: User,
    ) -> Salary:
        select_salary = select(Salary).where(Salary.user_id == user.id)
        result = await session.execute(select_salary)
        salary = result.scalar()
        return salary

    async def get_multi_salaries(self, session: AsyncSession) -> list[Salary]:
        select_salary = select(Salary).options(joinedload(Salary.user))
        result = await session.execute(select_salary)
        salaries = result.scalars().all()
        salary_list = []
        for salary in salaries:
            salary_data = SalaryGET(
                salary=salary.salary,
                raising_date=salary.raising_date,
                user_id=UserRead(
                    id=salary.user.id,
                    email=salary.user.email,
                    first_name=salary.user.first_name,
                    last_name=salary.user.last_name,
                ),
            )
            salary_list.append(salary_data)
        return salary_list


salary_crud = SalaryCRUD(Salary)
