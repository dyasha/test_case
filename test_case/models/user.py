from fastapi_users_db_sqlalchemy import SQLAlchemyBaseUserTable
from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from test_case.core.database.db import Base


class User(SQLAlchemyBaseUserTable[int], Base):
    """Модель пользователя."""

    first_name = Column(String, index=True)
    last_name = Column(String, index=True)
    salary = relationship("Salary", uselist=False, backref="user")
