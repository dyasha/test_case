from sqlalchemy import Column, Date, ForeignKey, Integer

from test_case.core.database.db import Base


class Salary(Base):
    """Модель зарплаты."""

    salary = Column(Integer, index=True)
    raising_date = Column(Date, index=True)
    user_id = Column(Integer, ForeignKey("user.id"), unique=True)
