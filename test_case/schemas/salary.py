from datetime import date, timedelta

from pydantic import BaseModel, Field, validator

from test_case.schemas.user import UserRead

EXAMPLE = date.today() + timedelta(weeks=50)


class SalaryUpdate(BaseModel):
    salary: int = Field(..., gt=17866, example=17867)  # МРОТ
    raising_date: date = Field(..., example=EXAMPLE)

    @validator("raising_date")
    def validate_raising_date(cls, value: date) -> date:
        if value <= date.today():
            raise ValueError("Дата повышения должна быть в будущем.")
        return value


class SalaryCreate(SalaryUpdate):
    user_id: int


class SalaryGET(SalaryUpdate):
    user_id: UserRead


class SalaryDB(SalaryCreate):
    id: int

    class Config:
        orm_mode = True
