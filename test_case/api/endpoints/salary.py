from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from test_case.api.validators import (
    check_salary_exists,
    salary_exists,
    salary_exists_info,
)
from test_case.core.database.db import get_async_session
from test_case.core.user import current_superuser, current_user
from test_case.crud.salary import salary_crud
from test_case.models.user import User
from test_case.schemas.salary import SalaryCreate, SalaryDB, SalaryGET, SalaryUpdate

router = APIRouter()


@router.get(
    "/",
    response_model=list[SalaryGET],
    dependencies=[Depends(current_superuser)],
)
async def get_all_salarys(session: AsyncSession = Depends(get_async_session)):
    """Только для суперюзеров. Просмотр всех зарплат."""
    all_salarys = await salary_crud.get_multi_salaries(session)
    return all_salarys


@router.get(
    "/my_salary",
    response_model=SalaryDB,
    response_model_exclude={"user_id", "id"},
)
async def get_my_salary(
    session: AsyncSession = Depends(get_async_session),
    user: User = Depends(current_user),
):
    """Получает список зарплаты для текущего пользователя."""
    await salary_exists_info(user.id, session)
    salary = await salary_crud.get_by_user(session, user)
    return salary


@router.post(
    "/",
    response_model=SalaryDB,
    dependencies=[Depends(current_superuser)],
)
async def create_new_salary(
    salary: SalaryCreate, session: AsyncSession = Depends(get_async_session)
):
    """Только для суперюзеров. Создание новой зарплаты."""
    await salary_exists(salary.user_id, session)
    new_salary = await salary_crud.create(salary, session)
    return new_salary


@router.patch(
    "/{salary_id}",
    response_model=SalaryDB,
    dependencies=[Depends(current_superuser)],
)
async def update_salary(
    salary_id: int,
    obj_in: SalaryUpdate,
    session: AsyncSession = Depends(get_async_session),
):
    """Только для суперюзеров. Обновление зарплаты."""
    salary = await check_salary_exists(salary_id, session)
    salary = await salary_crud.update(salary, obj_in, session)
    return salary


@router.delete(
    "/{salary_id}",
    response_model=SalaryDB,
    dependencies=[Depends(current_superuser)],
)
async def remove_salary(
    salary_id: int, session: AsyncSession = Depends(get_async_session)
):
    """Только для суперюзеров. Удаление зарплаты."""
    remove_salary = await check_salary_exists(salary_id, session)
    remove_salary = await salary_crud.remove(remove_salary, session)
    return remove_salary
