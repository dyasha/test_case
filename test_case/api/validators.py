from fastapi import HTTPException
from sqlalchemy import exists, select
from sqlalchemy.ext.asyncio import AsyncSession

from test_case.crud.salary import salary_crud
from test_case.models.salary import Salary


async def salary_exists(
    user_id: int,
    session: AsyncSession,
):
    """Проверяем, есть ли у этого юзера зарплата."""
    object_exists = await session.scalar(
        select(exists().where(Salary.user_id == user_id))
    )
    if object_exists is not False:
        raise HTTPException(
            status_code=422,
            detail="У этого пользователя уже есть зарплата." " Новую добавлять нельзя",
        )


async def salary_exists_info(
    user_id: int,
    session: AsyncSession,
):
    """Проверяем, есть ли у этого юзера зарплата и выводим сообщение."""
    object_exists = await session.scalar(
        select(exists().where(Salary.user_id == user_id))
    )
    if object_exists is False:
        raise HTTPException(
            status_code=203,
            detail="У вас пока что не установлена зарплата. "
            "Дождитесь, когда Администратор ее установит.",
        )


async def check_salary_exists(salary_id: int, session: AsyncSession):
    """Проверяем, существует ли зарплата"""
    object_exists = await salary_crud.get(salary_id, session)
    if object_exists is None:
        raise HTTPException(status_code=404, detail="Зарплата не найдена")
    return object_exists
