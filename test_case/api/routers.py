from fastapi import APIRouter

from test_case.api.endpoints import salary_router, user_router

main_router = APIRouter()

main_router.include_router(user_router)
main_router.include_router(salary_router, prefix="/salary", tags=["Salary"])
