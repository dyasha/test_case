from fastapi import FastAPI

from test_case.api.routers import main_router
from test_case.core.config import settings
from test_case.core.database.init_db import create_first_superuser

app = FastAPI(title=settings.app_title)


app.include_router(main_router)


@app.on_event("startup")
async def startup():
    await create_first_superuser()
