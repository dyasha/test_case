FROM python:3.11-slim-buster

RUN python -m pip install --upgrade pip

RUN apt-get update && apt-get install -y libpq-dev gcc postgresql-client

COPY . .

RUN pip install poetry==1.4.2
RUN poetry config virtualenvs.create false
RUN poetry install --without dev
RUN alembic upgrade head

CMD uvicorn test_case.main:app --host 0.0.0.0 --port 8000 --reload
